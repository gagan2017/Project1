﻿using System;
namespace Project1
{
	
	public abstract class Table : ITable
	{
		public static string query = "";
		public static string TableName = "";

		public virtual void create()
		{
			setQuery();
			ConnectionMySql.getInstance().CreateRow(TableName, query);
		}
		public abstract  void Delete();
		public abstract  void Display();
		public abstract void Update();
		public abstract void setQuery();

	}
}
