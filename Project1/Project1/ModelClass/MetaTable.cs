﻿using System;
namespace Project1
{
	public class UserMeta : Table
   {
		public int id;
		public UserKey LastUpdateBy { get; set; }
		public DateTime LastUpdated { get; set; }

		public UserMeta()
		{
			LastUpdated = new DateTime();
			LastUpdateBy = new UserKey();
			TableName = "Metadata(userid,LastUpdatedDate)";
		}
		public override void setQuery()
		{
			query = "(" + LastUpdateBy.Id + ",Date '" + DateTime.Now.Date + "')";

		}
      

		public override void Delete()
		{
			throw new NotImplementedException();
		}

		public override void Display()
		{
			var v = ConnectionMySql.getInstance().DisplayMetatable(id);
			id = v.id;
			LastUpdateBy = v.LastUpdateBy;
			LastUpdated = v.LastUpdated;
		}

		public override void Update()
		{
			throw new NotImplementedException();
		}
	}
}
