﻿using System;
namespace Project1
{
	public interface ITable
	{
		void create();
		void Delete();
		void Update();
		void Display();
	}
	public interface IDisplay
	{
		User DisplayUser(int id);
		DataTable DisplayDataTable(int id);
		UserMeta DisplayMetatable(int id);

	}

	public interface IConnectionBase : IDisplay
	{
		bool CreateRow(string pTableName, string pTabledata);
		bool DeleteRow(string pTableName, int pPrimaryKey);
		bool UpdateRow(string pTableName, string pUpdateQuery);
	}
}