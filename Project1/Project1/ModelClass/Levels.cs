﻿using System;
using System.Collections.Generic;

namespace Project1
{
	public enum ELevels
	{
		zero, one, two, three, four, five
	}
	public static class LevelOperation
	{
		static Dictionary<ELevels, string> leveldictionary = new Dictionary<ELevels, string>();
		static LevelOperation(){
			leveldictionary.Add(ELevels.zero, "0");
			leveldictionary.Add(ELevels.one, "1");
			leveldictionary.Add(ELevels.two, "2");
			leveldictionary.Add(ELevels.three, "3");
			leveldictionary.Add(ELevels.four, "4");
			leveldictionary.Add(ELevels.five, "5");
			}
		public static string getLevels(ELevels level)
		{

			return leveldictionary[level];
		}
        public static ELevels getLevelsasstring(string level)
        {

            foreach(var item in leveldictionary)
            {
                if (item.Value.Equals(level))
                {
                    return item.Key;
                }
            }
            return ELevels.zero;
        }

    }
}
