﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Project1.ModelClass
{
    public class UserMeta
    {
        public int id;
        public UserKey LastUpdateBy { get; set; }
        public DateTime LastUpdated { get; set; }
        string query = "";
        public bool CreateUserMeta()
        {
            query = "("+LastUpdateBy.Id+",Date '"+DateTime.Now.Date+"')" ;
            ConnectionMySql.getInstance().CreateRow("Metadata(userid,LastUpdatedDate)", query);
            return true;
        }
        public UserMeta Display()
        {
            
            return ConnectionMySql.getInstance().DisplayMetatable(id);
        }
        public bool DeleteUser()
        {
            query = "Delete";
            return true;
        }
        public bool UpdateUser()
        {
            query = "Alter";
            return true;
        }

    }
}