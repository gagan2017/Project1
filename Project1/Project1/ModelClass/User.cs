﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Project1
{
	
	public class User : Table
	{
        public User()
        {
            Key = new UserKey();
            Data = new UserData();
			TableName = "User(FirstName,LastName,email,password,levels)";
		}
		public UserKey Key { get; set; }
		public UserData Data { get; set; }
		public ELevels level;
		public override void setQuery()
		{
			query = "('" + Data.FirstName + "','" + Data.LastName + "','" + Data.Email + "','" + Data.Password + "','" + LevelOperation.getLevels(level) + "');";

		}

		public override void Display()
		{
           var v= ConnectionMySql.getInstance().DisplayUser(Key.Id);
			this.Key = v.Key;
			this.Data = v.Data;
			this.level = v.level;
		}
		public override void Delete()
		{
			query = "Delete";
            
		}
		public override void Update()
        {
            query = "Alter";
        }

    }

}
