﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Project1.ModelClass
{
    public interface IDisplay
    {
        User DisplayUser(int id);
        DataTable DisplayDataTable(int id);
        UserMeta DisplayMetatable(int id);

    }

    public interface IConnectionBase : IDisplay
    {
        bool CreateRow(string pTableName, string pTabledata);
        bool DeleteRow(string pTableName, int pPrimaryKey);

        bool UpdateRow(string pTableName, string pUpdateQuery);
    }
}