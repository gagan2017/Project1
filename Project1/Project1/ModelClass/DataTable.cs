﻿using System;
namespace Project1
{
	public class DataTable:Table
	{
		public DataTable()
		{
			TableName = "DataTable(Title,Description,metadataid)";
		}
		public int Id;
		public string Topic;
		public string Description;
		public int Metaid;
        public override void setQuery()
		{
			query = "('" + Topic + "','" + Description + "'," + Metaid + ")";
		}
		public override void Display()
		{
			var v = ConnectionMySql.getInstance().DisplayDataTable(Id);
			Id = v.Id;
			Topic = v.Topic;
			Description = v.Description;
			Metaid = v.Metaid;
		}

		public override void Update()
		{
			throw new NotImplementedException();
		}
		public override void Delete()
		{
			throw new NotImplementedException();
		}
	}
}
