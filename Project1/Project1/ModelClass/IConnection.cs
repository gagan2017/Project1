﻿using System;
using MySql.Data.MySqlClient;

namespace Project1
{
	
	public sealed class ConnectionMySql : IConnectionBase
	{
		private static ConnectionMySql _Instance = new ConnectionMySql();
		MySqlConnection connection =new MySqlConnection(Constants.Dburl);
		MySqlCommand cmd=new MySqlCommand();



		public static ConnectionMySql getInstance()
		{
			return _Instance;
		}

		public bool CreateRow(string pTableName,string pTabledata)
		{
			try
			{
				_Instance.connection.Open();

				_Instance.cmd.CommandText = Constants.Create(pTableName, pTabledata);

				_Instance.cmd.Connection = _Instance.connection;
				_Instance.cmd.ExecuteNonQuery();
				_Instance.connection.Close();
			}
			catch (Exception e)
			{
				return false;
			}
			finally
			{
				_Instance.connection.Close();

			}
			return true;
		}

		/*public MySqlDataReader Display(string pTableName)
		{
			_Instance.connection.Open();

			_Instance.cmd.CommandText = Constants.Display(pTableName);
			_Instance.cmd.Connection = _Instance.connection;
			var Reader=_Instance.cmd.ExecuteReader();

			while (Reader.Read())
			{
				var s = Reader[1];
				var e = Reader[3];
			}
				_Instance.connection.Close();
			return Reader;

		}*/
		bool IConnectionBase.DeleteRow(string pTableName, int pPrimaryKey)
		{
			try
			{
				_Instance.connection.Open();

				_Instance.cmd.CommandText = Constants.Delete(pTableName, pPrimaryKey);

				_Instance.cmd.Connection = _Instance.connection;
				_Instance.cmd.ExecuteNonQuery();
			}
			finally
			{
				_Instance.connection.Close();

			}
			return true;
		}

		bool IConnectionBase.UpdateRow(string pTableName, string pUpdateQuery)
		{
			try
			{
				_Instance.connection.Open();
				_Instance.cmd.CommandText = Constants.Update(pTableName, pUpdateQuery);
				_Instance.cmd.Connection = _Instance.connection;
				_Instance.cmd.ExecuteNonQuery();
				_Instance.connection.Close();
			}
			finally
			{
				_Instance.connection.Close();

			}
			return true;
		}

        public User DisplayUser(int id)
        {
			try
			{
				_Instance.connection.Open();

				_Instance.cmd.CommandText = Constants.Display("user", id);
				_Instance.cmd.Connection = _Instance.connection;
				var Reader = _Instance.cmd.ExecuteReader();
				User user = new User();
				while (Reader.Read())
				{
					user.Key.Id = int.Parse(Reader[0].ToString());
					user.Data.FirstName = Reader[1].ToString();
					user.Data.LastName = Reader[2].ToString();
					user.Data.Email = Reader[3].ToString();
					user.Data.Password = Reader[3].ToString();
					user.level = LevelOperation.getLevelsasstring(Reader[3].ToString());


				}
				return user;
			}
			         

			finally
			{
				_Instance.connection.Close();

			}
		}

        public DataTable DisplayDataTable(int id)
        {
			try
			{
				_Instance.connection.Open();

				_Instance.cmd.CommandText = Constants.Display("user", id);
				_Instance.cmd.Connection = _Instance.connection;
				var Reader = _Instance.cmd.ExecuteReader();
				DataTable datatable = new DataTable();
				while (Reader.Read())
				{
					datatable.Id = int.Parse(Reader[0].ToString());
					datatable.Topic = Reader[1].ToString();
					datatable.Description = Reader[2].ToString();
					datatable.Metaid = int.Parse(Reader[3].ToString());


				}
				_Instance.connection.Close();
				return datatable;
			}
			finally
			{
				_Instance.connection.Close();

			}
        }

        public UserMeta DisplayMetatable(int id)
        {
			try
			{
				_Instance.connection.Open();

				_Instance.cmd.CommandText = Constants.Display("meta", id);
				_Instance.cmd.Connection = _Instance.connection;
				var Reader = _Instance.cmd.ExecuteReader();
				UserMeta usermeta = new UserMeta();
				while (Reader.Read())
				{
					usermeta.id = int.Parse(Reader[0].ToString());
					usermeta.LastUpdateBy.Id = int.Parse(Reader[1].ToString());
					usermeta.LastUpdated = (DateTime)Reader[2];
				}
				_Instance.connection.Close();
				return usermeta;
			}
			finally
			{
				_Instance.connection.Close();

			}
        }
    }
}
