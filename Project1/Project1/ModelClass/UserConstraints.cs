﻿using System;
namespace Project1
{
	public class UserKey
	{
		public int Id { get; set; }
	}
	public class UserData
	{
		public string FirstName { get; set; }
		public string LastName { get; set; }

		public string Email { get; set; }
		public string Password { get; set; }
	}
	
}
