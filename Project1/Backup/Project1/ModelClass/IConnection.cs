﻿using System;
using MySql.Data.MySqlClient;

namespace Project1
{
	public interface IDisplay
	{
		User DisplayUser();
		DataTable DisplayDataTable();
		UserMeta DisplayMetatable();

	}

	public interface IConnectionBase 
	{
		bool CreateRow(string pTableName,string pTabledata);
		bool DeleteRow(string pTableName, int pPrimaryKey);

		bool UpdateRow(string pTableName,string pUpdateQuery);
	}
	public sealed class ConnectionMySql : IConnectionBase
	{
		private static ConnectionMySql _Instance = new ConnectionMySql();
		MySqlConnection connection =new MySqlConnection(Constants.Dburl);
		MySqlCommand cmd=new MySqlCommand();



		public static ConnectionMySql getInstance()
		{
			return _Instance;
		}

		public bool CreateRow(string pTableName,string pTabledata)
		{
			try
			{
				_Instance.connection.Open();

				_Instance.cmd.CommandText = Constants.Create(pTableName, pTabledata);

				_Instance.cmd.Connection = _Instance.connection;
				_Instance.cmd.ExecuteNonQuery();
				_Instance.connection.Close();
			}
			catch (Exception e)
			{
				return false;
			}
			return true;
		}

		public MySqlDataReader Display(string pTableName)
		{
			_Instance.connection.Open();

			_Instance.cmd.CommandText = Constants.Display(pTableName);
			_Instance.cmd.Connection = _Instance.connection;
			var Reader=_Instance.cmd.ExecuteReader();

			while (Reader.Read())
			{
				var s = Reader[1];
				var e = Reader[3];
			}
				_Instance.connection.Close();
			return Reader;

		}
		bool IConnectionBase.DeleteRow(string pTableName, int pPrimaryKey)
		{
			_Instance.connection.Open();

			_Instance.cmd.CommandText = Constants.Delete(pTableName, pPrimaryKey);

			_Instance.cmd.Connection = _Instance.connection;
			_Instance.cmd.ExecuteNonQuery();

			_Instance.connection.Close();
			return true;
		}

		bool IConnectionBase.UpdateRow(string pTableName, string pUpdateQuery)
		{
			_Instance.connection.Open();
			_Instance.cmd.CommandText = Constants.Update(pTableName, pUpdateQuery);
			_Instance.cmd.Connection = _Instance.connection;
			_Instance.cmd.ExecuteNonQuery();
			_Instance.connection.Close();
			return true;
		}
	}
}
